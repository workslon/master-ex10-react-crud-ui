jest.dontMock('../src/js/components/CreateBook.react');
jest.dontMock('../src/js/constants/StatusConstants');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');
var AppActions = require('../src/js/actions/AppActions');
var StatusConstants = require('../src/js/constants/StatusConstants');

describe('CreateBook.react', function () {
  var CreateBook = require('../src/js/components/CreateBook.react');
  var renderer = TestUtils.createRenderer();
  var result;
  var children;

  it('renders "CreateBook" view correctly', function () {
    renderer.render(<CreateBook />);
    result = renderer.getRenderOutput();
    children = result.props.children;
    // header
    expect(children[0].type).toEqual('h3');
    // ISBN label
    expect(children[1].props.children[0].type).toEqual('label');
    expect(children[1].props.children[0].props.children).toEqual('ISBN');
    // ISBN input
    expect(children[1].props.children[1].type).toEqual('input');
    expect(children[1].props.children[1].ref).toEqual('isbn');
    expect(children[1].props.children[1].props.defaultValue).toEqual('');
    expect(children[1].props.children[1].props.type).toEqual('text');
    // Title label
    expect(children[2].props.children[0].type).toEqual('label');
    expect(children[2].props.children[0].props.children).toEqual('Title');
    // Title input
    expect(children[2].props.children[1].type).toEqual('input');
    expect(children[2].props.children[1].ref).toEqual('title');
    expect(children[2].props.children[1].props.defaultValue).toEqual('');
    expect(children[2].props.children[1].props.type).toEqual('text');
    // Year label
    expect(children[3].props.children[0].type).toEqual('label');
    expect(children[3].props.children[0].props.children).toEqual('Year');
    // Year input
    expect(children[3].props.children[1].type).toEqual('input');
    expect(children[3].props.children[1].ref).toEqual('year');
    expect(children[3].props.children[1].props.defaultValue).toEqual('');
    expect(children[3].props.children[1].props.type).toEqual('text');
    // button
    expect(children[4].type).toEqual('button');
    expect(children[4].props.onClick).toBeDefined();
    expect(children[4].props.children).toEqual('Submit');
  });

  it('calls "AppActions.createBook" when clicking on "Submit" button', function () {
    renderer.render(<CreateBook />);
    result = renderer.getRenderOutput();
    children = result.props.children;
    // as the "createBook" function uses native "e.preventDefault" we need to simulate it.
    var fakeEvent = {
      preventDefault: function () {}
    };

    children[4].props.onClick(fakeEvent);
    expect(AppActions.createBook).toBeCalled();
  });

  it('should render the error message if there are some validation errors', function () {
    var notifications = {
      errors: {
        isbn: 'ISBN error',
        title: 'Title error',
        year: 'Year error'
      }
    };

    renderer.render(<CreateBook notifications={notifications} />);
    result = renderer.getRenderOutput();
    children = result.props.children;

    // isbn
    expect(children[1].props.children[2].type).toEqual('span');
    expect(children[1].props.children[2].props.children).toEqual('ISBN error');
    // title
    expect(children[2].props.children[2].type).toEqual('span');
    expect(children[2].props.children[2].props.children).toEqual('Title error');
    // year
    expect(children[3].props.children[2].type).toEqual('span');
    expect(children[3].props.children[2].props.children).toEqual('Year error');
  });

  it('should show success notification if a book was successfully created', function () {
    var notifications = {
        status: StatusConstants.SUCCESS
    };

    renderer.render(<CreateBook notifications={notifications} />);
    result = renderer.getRenderOutput();
    children = result.props.children;

    expect(children[5].type).toBe('p');
    expect(children[5].props.children).toBe('Success!');
  });

  it('should show pending notification while saving a book', function() {
    var notifications = {
        status: StatusConstants.PENDING
    };

    renderer.render(<CreateBook notifications={notifications} />);
    result = renderer.getRenderOutput();
    children = result.props.children;

    expect(children[6].type).toBe('p');
    expect(children[6].props.children).toBe('Creating...');
  });
});