var AppDispatcher = require('../dispatchers/AppDispatcher');
var ActionConstants = require('../constants/ActionConstants');
var StatusConstants = require('../constants/StatusConstants');
var EventEmitter = require('events').EventEmitter;
var eNUMERATION = require('eNUMERATION');
var Model = require('../models/model');

var CHANGE_EVENT = 'change';

var records = [];
var notifications = {
  status: StatusConstants.IDLE,
  errors: {}
};

var AppStore = Object.assign({}, EventEmitter.prototype, {
  getRecord: function getBook(id) {
    return records.filter(function (record) {
      return record.objectId === id;
    })[0];
  },

  getAllRecords: function () {
    return records;
  },

  getNotifications: function () {
    return notifications;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  switch(action.type) {
    // -- Get all records
    case ActionConstants.REQUEST_RECORDS_SUCCESS:
      try {
        records = action.result;
        AppStore.emitChange();
      } catch (e) {
        alert('Unvalid remote response format!');
      }
      break;

    // -- Create record Pending
    case ActionConstants.REQUEST_RECORD_SAVE:
      notifications.status = StatusConstants.PENDING;
      AppStore.emitChange();
      break;

    // -- Create record Success
    case ActionConstants.RECORD_SAVE_SUCCESS:
      try {
        action.data.objectId = JSON.parse(action.result).objectId;
        records.push(action.data);
        notifications.errors = {};
        notifications.status = StatusConstants.SUCCESS;
        AppStore.emitChange();
      } catch (e) {}
      break;

    // -- Create record Error
    case ActionConstants.RECORD_SAVE_ERROR:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = action.error;
      AppStore.emitChange();
      break;

    // -- Update record Pending
    case ActionConstants.REQUEST_RECORD_UPDATE:
      notifications.status = StatusConstants.PENDING;
      AppStore.emitChange();
      break;

    // --- Update record Success
    case ActionConstants.RECORD_UPDATE_SUCCESS:
      records = records.map(function (record) {
        if (record.objectId === action.data.objectId) {
          for (var prop in Model.properties) {
            record[prop] = action.data[prop];
          }
        }
        return record;
      });
      notifications.errors = {};
      notifications.status = StatusConstants.SUCCESS;
      AppStore.emitChange();
      break;

    // -- Update record Error
    case ActionConstants.RECORD_UPDATE_ERROR:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = action.error;
      AppStore.emitChange();
      break;

    // -- Destroy record Pending
    case ActionConstants.REQUEST_RECORD_DESTROY:
      notifications.status = StatusConstants.PENDING;
      AppStore.emitChange();
      break;

    // -- Destroy record Success
    case ActionConstants.RECORD_DESTROY_SUCCESS:
      records = records.filter(function (record) {
        return record.objectId !== action.data.objectId;
      });
      notifications.status = StatusConstants.IDLE;
      AppStore.emitChange();
      break;

    // --- Client Validation error
    case ActionConstants.RECORD_VALIDATION_ERROR:
      Object.keys(action.errors).map(function (key) {
        notifications.errors[key] = action.errors[key];
      });

      notifications.status = StatusConstants.ERROR;
      AppStore.emitChange();
      break;

    // --- Non-unique STDID
    case ActionConstants.NON_UNIQUE_STDID:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = {
        isbn: 'The record with such STDID already exists!'
      };
      AppStore.emitChange();
      break;

    // --- Clear all notifications (eather errors or success)
    case ActionConstants.CLEAR_NOTIFICATIONS:
      notifications = {
        status: StatusConstants.IDLE,
        errors: {}
      };
      AppStore.emitChange();
      break;
  }
});

module.exports = AppStore;