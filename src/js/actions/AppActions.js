var AppDispatcher = require('../dispatchers/AppDispatcher');
var ActionConstants = require('../constants/ActionConstants');
var adapter = require('../storage-managers/ParseApiAdapter');

module.exports = {
  validate: function(modelClass, key, value) {
    var errors = {};
    errors[key] = modelClass.check(key, value).message;

    AppDispatcher.dispatch({
      type: ActionConstants.RECORD_VALIDATION_ERROR,
      errors: errors
    });
  },

  getRecords: function (modelClass) {
    var promise = adapter.retrieveAll(modelClass);

    AppDispatcher.dispatchAsync(promise, {
      request: ActionConstants.REQUEST_RECORDS,
      success: ActionConstants.REQUEST_RECORDS_SUCCESS,
      failure: ActionConstants.REQUEST_RECORDS_ERROR
    });
  },

  createRecord: function (modelClass, data) {
    var modelProps = modelClass.properties;
    var stdId = Object.keys(modelProps).reduce(function (current, next) {
      if (modelProps[current].isStandardId) {
        return current;
      }
    });
    var obj = {};

    obj[stdId] = data[stdId];

    adapter
      .retrieve(modelClass, '', {where: obj})
      .then(function(response) {
        try {
          if (response.results.length) {
            AppDispatcher.dispatch({
              type: ActionConstants.NON_UNIQUE_STDID
            });
          } else {
            data.runCloudCode = true;
            AppDispatcher.dispatchAsync(adapter.add(modelClass, data), {
              request: ActionConstants.REQUEST_RECORD_SAVE,
              success: ActionConstants.RECORD_SAVE_SUCCESS,
              failure: ActionConstants.RECORD_SAVE_ERROR
            }, data);
          }
        } catch(e) {}
      });

    AppDispatcher.dispatch({
      type: ActionConstants.REQUEST_RECORD_SAVE
    });
  },

  updateRecord: function (modelClass, record, newData) {
    var promise;

    newData.runCloudCode = true;
    promise = adapter.update(modelClass, record.objectId, newData);
    newData.objectId = record.objectId;

    AppDispatcher.dispatchAsync(promise, {
      request: ActionConstants.REQUEST_RECORD_UPDATE,
      success: ActionConstants.RECORD_UPDATE_SUCCESS,
      failure: ActionConstants.RECORD_UPDATE_ERROR
    }, newData);
  },

  deleteRecord: function (modelClass, record) {
    var promise;

    record.runCloudCode = true;
    promise = adapter.destroy(modelClass, record.objectId);

    AppDispatcher.dispatchAsync(promise, {
      request: ActionConstants.REQUEST_RECORD_DESTROY,
      success: ActionConstants.RECORD_DESTROY_SUCCESS,
      failure: ActionConstants.RECORD_DESTROY_ERROR
    }, record);
  },

  clearNotifications: function () {
    AppDispatcher.dispatch({
      type: ActionConstants.CLEAR_NOTIFICATIONS
    });
  }
};