var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var StatusConstants = require('../constants/StatusConstants');
var IndexLink = require('../../../node_modules/react-router/lib/IndexLink');
var Model = require('../models/model');
var eNUMERATION = require('eNUMERATION');

module.exports = React.createClass({
  displayName: 'CreateRecord',

  componentDidUpdate: function () {
    var notifications = this.props.notifications || {};
    var status = notifications.status;

    if (status === StatusConstants.SUCCESS) {
      var refs = this.refs;

      for (var key in refs) {
        refs[key].value = '';
      }
    }
  },

  _createRecord: function (e) {
    e.preventDefault();

    var modelProps = Model.properties;
    var refs = this.refs || {};
    var fields = {};

    for (var key in refs) {
      if (modelProps[key].range && modelProps[key].range === 'Integer') {
        fields[key] = parseInt(refs[key].value);
      } else {
        fields[key] = refs[key].value;
      }
    }

    AppActions.createRecord(Model, fields);
  },

  _validate: function(e) {
    AppActions.validate(Model, e.target.id, e.target.value);
  },

  render: function () {
    var modelProps = Model.properties;
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
      <form>
        <h3>Create {Model.name}</h3>
        {Object.keys(modelProps).map((function (prop) {
          if (modelProps[prop].range && modelProps[prop].range instanceof eNUMERATION) {
            return (
              <div key={prop} className="form-group">
                <label forHtml={prop}>{modelProps[prop].label}</label>
                <select className="form-control" ref={prop} id={prop}>
                  {modelProps[prop].range.labels.map(function (label) {
                    return <option key={label} defaultValue={label}>{label}</option>
                  })}
                </select>
              </div>
            );
          } else {
            return (
              <div key={prop} className="form-group">
                <label forHtml={prop}>{modelProps[prop].label}</label>
                <input defaultValue="" onChange={this._validate} ref={prop} type="text" className="form-control" id={prop} placeholder={modelProps[prop].label} />
                {errors[prop] && <span className="text-danger">{errors[prop]}</span>}
              </div>
            );
          }
        }).bind(this))}
        <button type="submit" onClick={this._createRecord} className="btn btn-default">Submit</button>
        {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
        {status === StatusConstants.PENDING && <p className="bg-info">Creating...</p>}
        <IndexLink className="back" to="/">&laquo; back</IndexLink>
      </form>
    );
  }
});