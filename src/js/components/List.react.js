var React = require('react');
var StatusConstants = require('../constants/StatusConstants');
var Link = require('../../../node_modules/react-router/lib/Link');
var Record = require('./Record.react');
var Model = require('../models/model');

module.exports = React.createClass({
  displayName: 'List',

  render: function () {
    var modelProps = Model.properties;
    var stdIdPropName = Object.keys(modelProps).reduce(function (current, next) {
      if (modelProps[current].isStandardId) {
        return current;
      } else {
        return next;
      }
    });
    var notifications = this.props.notifications || {};
    var status = notifications.status;

    return (
      <div>
        <Link className="create-book btn btn-success bt-sm" to="/create">+ Add {Model.name}</Link>
        {status === StatusConstants.PENDING && <p className="bg-info">Deleting...</p>}
        {this.props.records.length ?
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                {Object.keys(modelProps).map(function (prop) {
                  return <th key={prop}>{modelProps[prop].label}</th>
                })}
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.props.records.map((function (record, i) {
                return (
                  <Record key={record.objectId} nr={i + 1} record={record} />
                );
              }).bind(this))}
            </tbody>
          </table>
          : <div>Loading records...</div>}
      </div>
    );
  }
});