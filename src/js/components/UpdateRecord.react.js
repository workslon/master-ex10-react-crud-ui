var React = require('react');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');
var StatusConstants = require('../constants/StatusConstants');
var IndexLink = require('../../../node_modules/react-router/lib/IndexLink');
var Model = require('../models/model');
var eNUMERATION = require('eNUMERATION');

module.exports = React.createClass({
  displayName: 'UpdateRecord',

  componentWillMount: function() {
    this.record = AppStore.getRecord(this.props.params.id);
  },

  _updateRecord: function (e) {
    e.preventDefault();

    var modelProps = Model.properties;
    var refs = this.refs || {};
    var fields = {};

    for (var key in refs) {
      if (modelProps[key].range && modelProps[key].range === 'Integer') {
        fields[key] = parseInt(refs[key].value);
      } else {
        fields[key] = refs[key].value;
      }
    }

    AppActions.updateRecord(Model, this.record, fields);
  },

  _validate: function(e) {
    AppActions.validate(Model, e.target.id, e.target.value);
  },

  render: function () {
    var modelProps = Model.properties;
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
        <div>
          <h3>Update {Model.name}</h3>
          {this.record ?
            <form>
              {Object.keys(modelProps).map((function (prop) {
                if (modelProps[prop].range && modelProps[prop].range instanceof eNUMERATION) {
                  return (
                    <div key={prop} className="form-group">
                      <label forHtml={prop}>{this.record[prop]}</label>
                      <select defaultValue={this.record[prop]} className="form-control" ref={prop} id={prop}>
                        {modelProps[prop].range.labels.map(function (label) {
                          return <option key={label} value={label}>{label}</option>
                        })}
                      </select>
                    </div>
                  );
                } else {
                  return (
                    <div key={prop} className="form-group">
                      <label forHtml={prop}>{modelProps[prop].label}</label>
                      {modelProps[prop].isStandardId ?
                        <input disabled defaultValue={this.record[prop]} onChange={this._validate} ref={prop} type="text" className="form-control" id={prop} placeholder={modelProps[prop].label} /> :
                        <input defaultValue={this.record[prop]} onChange={this._validate} ref={prop} type="text" className="form-control" id={prop} placeholder={prop} />
                      }
                      {errors[prop] && <span className="text-danger">{errors[prop]}</span>}
                    </div>
                  );
                }
              }).bind(this))}
              <button type="submit" onClick={this._updateRecord} className="btn btn-default">Submit</button>
              {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
              {status === StatusConstants.PENDING && <p className="bg-info">Updating...</p>}
              <IndexLink className="back" to="/">&laquo; back</IndexLink>
            </form>
          : <div>No book found...</div>}
        </div>
    );
  }
});