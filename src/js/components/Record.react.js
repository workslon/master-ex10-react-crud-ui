var React = require('react');
var Link = require('react-router').Link;
var AppActions = require('../actions/AppActions');
var Model = require('../models/model');

module.exports = React.createClass({
  displayName: 'Record',

  _deleteRecord: function () {
    AppActions.deleteRecord(Model, this.props.record);
  },

  render: function () {
    var modelProps = Model.properties;
    var record = this.props.record;
    var updatePath = '/update/' + record.objectId;

    return (
      <tr>
        <td scope="row">{this.props.nr}</td>
        {Object.keys(modelProps).map(function (prop) {
          return <td key={prop}>{record[prop]}</td>
        })}
        <td className="action-links">
          <Link className="btn btn-primary btn-xs" to={updatePath}>Update</Link>
          <a className="btn btn-danger btn-xs" onClick={this._deleteRecord}>Delete</a>
        </td>
      </tr>
    );
  }
});

