var React = require('react');
var IndexLink = require('../../../node_modules/react-router/lib/IndexLink');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');
var StatusConstants = require('../constants/StatusConstants');
var Model = require('../models/model.js');

module.exports = React.createClass({
  displayName: 'App',

  getInitialState: function () {
    return {
      records: AppStore.getAllRecords(),
      notifications: AppStore.getNotifications()
    }
  },

  childContextTypes: {
    router: React.PropTypes.object.isRequired
  },

  contextTypes: {
    router: React.PropTypes.object.isRequired
  },

  componentDidMount: function () {
    AppActions.getRecords(Model);
    this.context.router.listenBefore(this._clearNotifications);
    AppStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function () {
    AppStore.removeChangeListener(this._onChange);
  },

  _clearNotifications: function () {
    var notifications = this.state.notifications;
    var isStatus = notifications.status !== StatusConstants.IDLE;
    var isErrors = Object.keys(notifications.errors || {}).length;

    if (isStatus || isErrors) {
      AppActions.clearNotifications();
    }
  },

  _onChange: function () {
    this.setState({
      records: AppStore.getAllRecords(),
      notifications: AppStore.getNotifications()
    });
  },

  _renderChildren: function () {
    return React.Children.map(this.props.children, (function (child) {
      var clone = React.cloneElement(child, {
        records: this.state.records,
        notifications: this.state.notifications
      });

      child = null;

      return clone;
    }).bind(this));
  },

  render: function () {
    var headerWordfing = Model.name + 's\' Manager';

    return (
      <div>
        <div className="page-header">
          <h1><IndexLink to="/">{headerWordfing}</IndexLink></h1>
        </div>
        { this._renderChildren() }
      </div>
    );
  }
});

