var mODELcLASS = require('mODELcLASS');
var eNUMERATION = require('eNUMERATION');

module.exports = new mODELcLASS({
  name: 'Book',
  properties: {
    isbn: {
      label: 'ISBN',
      range: 'NonEmptyString',
      isStandardId: true,
      pattern: /\b\d{9}(\d|X)\b/,
      patternMessage: 'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!'
    },
    title: {
      label: 'Title',
      range: 'NonEmptyString',
      min: 2,
      max: 50
    },
    year: {
      label: 'Year',
      range: 'Integer',
      min: 1459,
      max: (new Date()).getFullYear()
    },
    genre: {
      label: "Genre",
      range: new eNUMERATION('Genre', ['Documentary', 'Fiction', 'Detective']),
      optional: true
    }
  }
});
// var mODELcLASS = require('mODELcLASS');

// module.exports = new mODELcLASS({
//   name: 'Movie',
//   properties: {
//     title: {
//       label: 'Title',
//       range: 'NonEmptyString',
//       isStandardId: true,
//       min: 2,
//       max: 50
//     },
//     director: {
//       label: 'Director',
//       range: 'NonEmptyString',
//       min: 2,
//       max: 50
//     },
//     year: {
//       label: 'Year',
//       range: 'Integer',
//       min: 1459,
//       max: (new Date()).getFullYear()
//     }
//   }
// });
// var mODELcLASS = require('mODELcLASS');

// module.exports = new mODELcLASS({
//   name: 'Person',
//   properties: {
//     name: {
//       label: 'Name',
//       range: 'NonEmptyString',
//       isStandardId: true,
//       min: 2,
//       max: 50
//     },
//     surname: {
//       label: 'Surname',
//       range: 'NonEmptyString',
//       min: 2,
//       max: 50
//     },
//     age: {
//       label: 'Age',
//       range: 'Integer',
//       min: 0,
//       max: 120
//     }
//   }
// });
// var mODELcLASS = require('mODELcLASS');