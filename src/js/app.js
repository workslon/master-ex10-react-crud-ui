var React = require('react');
var ReactDOM = require('react-dom');

var Router = require('../../node_modules/react-router/lib/Router');
var Route = require('../../node_modules/react-router/lib/Route');
var IndexRoute = require('../../node_modules/react-router/lib/IndexRoute');
var hashHistory = require('../../node_modules/react-router/lib/hashHistory');

var App = require('./components/App.react');
var List = require('./components/List.react');
var CreateRecord = require('./components/CreateRecord.react');
var UpdateRecord = require('./components/UpdateRecord.react');

ReactDOM.render((
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={List} />
      <Route path="/create" component={CreateRecord} />
      <Route path="/update/:id" component={UpdateRecord} />
    </Route>
  </Router>
), document.getElementById('app'));